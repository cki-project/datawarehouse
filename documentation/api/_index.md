---
title: REST API
weight: 10
aliases: [/l/datawarehouse/api, /l/datawarehouse-api]
---

CKI supports a Python client that facilitates both consuming the DataWarehouse API
and authenticating to it called [datawarehouse-api-lib].

[datawarehouse-api-lib]: https://gitlab.com/cki-project/datawarehouse-api-lib.git

## Pagination

Requests that return multiple items will be paginated by 30 items by default.
You can change this using the ?limit and ?offset parameters.
You can also set custom page sizes up to 100 using the ?limit parameter.

More information is available in the [LimitOffsetPagination docs].

[LimitOffsetPagination docs]: https://www.django-rest-framework.org/api-guide/pagination/#limitoffsetpagination
