---
title: Actions
---

Actions are used to notify DataWarehouse about actions perfomed, usually by
micro services, on KCIDB objects.

## Set triaged

Set last_triaged_at value to notify the object was triaged.
Successive calls refresh the value.

`POST /api/1/kcidb/$object_kind/$object_id/actions/triaged`

| Name | Type | Required | Description |
|------|------|----------|-------------|
| `object_kind` | `str` | Yes | `checkouts`, `builds` or `tests`. |
| `object_id` | `str/int` | Yes | `id` or `iid` of the object to modify. |
