"""Test the rabbitmq module."""
from unittest import mock

from django import test
from freezegun import freeze_time
import pika

from datawarehouse import message_rabbitmq
from datawarehouse import models


class MessageQueueTest(test.TestCase):
    """Unit tests for the MessageQueue module."""

    @freeze_time("2000-01-01T00:00:00")
    @mock.patch('datawarehouse.message_rabbitmq.settings.RABBITMQ_SEND_ENABLED', True)
    def setUp(self):
        """Set Up."""
        self.messagequeue = message_rabbitmq.MessageQueue()
        self.messagequeue.bulk_add('status', 'object_type', list(range(3)), {})

    def test_bulk_add(self):
        """Test add method."""
        self.assertEqual(3, models.MessagePending.objects.count())
        self.assertEqual([{
            'timestamp': '2000-01-01T00:00:00+00:00',
            'status': 'status',
            'object_type': 'object_type',
            'misc': {},
            'object': i,
        } for i in range(3)], list(
            models.MessagePending.objects
            .filter(kind=models.MessageKindEnum.RABBITMQ)
            .values_list('content', flat=True)
        ))

    def test_send(self):
        """Test send method. Queue many messages and send them at once."""
        self.messagequeue.queue = mock.MagicMock()

        # Send all the messages.
        self.messagequeue.send()
        self.messagequeue.queue.send_message.assert_has_calls([
            mock.call(
                {
                    'timestamp': '2000-01-01T00:00:00+00:00',
                    'status': 'status',
                    'object_type': 'object_type',
                    'misc': {},
                    'object': i,
                },
                'datawarehouse.object_type.status',
                exchange='cki.exchange.datawarehouse.kcidb',
                headers={'message-type': 'datawarehouse'},
                priority=1,
            )
            for i in range(3)
        ])

        # No messages pending.
        self.assertEqual(0, models.MessagePending.objects.count())

        # No messages in queue.
        self.messagequeue.queue.send_message.reset_mock()
        self.messagequeue.send()
        self.assertFalse(self.messagequeue.queue.send_message.called)

    @mock.patch('datawarehouse.message_rabbitmq.pika.BlockingConnection', mock.Mock())
    def test_send_fails(self):
        # pylint: disable=protected-access
        """Test send method fails. Messages are kept in queue."""
        self.messagequeue.queue = mock.MagicMock()
        self.messagequeue.queue.send_message.side_effect = pika.exceptions.AMQPError()

        # Send all the messages. All fail.
        with self.assertLogs(logger=message_rabbitmq.LOGGER, level="ERROR") as log_ctx:
            self.messagequeue.send()

        expected_log = f"ERROR:{message_rabbitmq.LOGGER.name}:Error sending message, will be retried\nTraceback"
        self.assertEqual(sum(log.startswith(expected_log) for log in log_ctx.output), 1, "Log one error and halt")

        # All messages still pending.
        self.assertEqual(3, models.MessagePending.objects.count())

    @mock.patch('datawarehouse.message_rabbitmq.pika.BlockingConnection', mock.Mock())
    def test_send_fails_middle(self):
        # pylint: disable=protected-access
        """Test send method fails while sending. Messages are kept in queue."""
        def mock_publish(data, _, **__):
            """Mock publish. Fail on msg 1."""
            if data['object'] == 1:
                raise pika.exceptions.AMQPError()

        self.messagequeue.queue = mock.MagicMock()
        self.messagequeue.queue.send_message = mock_publish

        # Send all the messages. Some fail.
        with self.assertLogs(logger=message_rabbitmq.LOGGER, level="ERROR") as log_ctx:
            self.messagequeue.send()

        expected_log = f"ERROR:{message_rabbitmq.LOGGER.name}:Error sending message, will be retried\nTraceback"
        self.assertEqual(sum(log.startswith(expected_log) for log in log_ctx.output), 1, "Log one error and halt")

        # Some messages still pending.
        self.assertQuerySetEqual([1, 2], models.MessagePending.objects.values_list("content__object", flat=True))
