"""Test scripts/issues.py."""
import datetime
from unittest import mock

from django.conf import settings
import django.contrib.auth.models as auth_models
from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import cron
from datawarehouse import models
from datawarehouse import scripts
from datawarehouse.scripts import issues
from datawarehouse.utils import datetime_bool
from tests import utils

DW_KCIDB_SCHEMA_VERSION = {
    "version": {
        "major": settings.PRODUCER_KCIDB_SCHEMA.major,
        "minor": settings.PRODUCER_KCIDB_SCHEMA.minor,
    }
}


class TestNotifyCheckoutIssueOccurrencesChanged(utils.TestCase):
    """Test notify_checkout_issueoccurrences_changed."""

    fixtures = (
        "tests/fixtures/basic.yaml",
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/issues_all_basic_policies.yaml",
        "tests/fixtures/multiple_issue_occurrences.yaml",
    )

    def setUp(self):
        """Delete scheduled task created by fixtures' signals."""
        models.QueuedTask.objects.all().delete()
        self.checkout = models.KCIDBCheckout.objects.get(id="redhat:public_checkout_1")

    @freeze_time("2010-01-02 09:00:00")
    @mock.patch("datawarehouse.utils.MSG_QUEUE.bulk_add")
    def test_notify_issueoccurrences_changed_schedules_msg_about_incidents(self, mocked_bulk_add):
        """Test scripts.notify_issueoccurrences_changed schedules a message about issues and incidents for KCIDB."""
        cases = [
            (
                models.KCIDBTestResult.objects.get(id="redhat:public_testresult_1"),
                models.Issue.objects.get(description="Issue Public", kind__tag="Kernel Bug"),
                {
                    "id": "redhat:issue_1",
                    "origin": "redhat",
                    "comment": "Issue Public",
                    "report_url": "https://issue.public",
                    "version": 1609463420,
                    "culprit": {"code": True, "tool": False, "harness": False},
                    "misc": {"is_public": True, "kcidb": DW_KCIDB_SCHEMA_VERSION},
                },
                {
                    "id": "redhat:incident_5",
                    "origin": "redhat",
                    "issue_id": "redhat:issue_1",
                    "issue_version": 1609463420,
                    "test_id": "redhat:public_test_2",
                    "present": True,
                    "misc": {"is_public": True, "kcidb": DW_KCIDB_SCHEMA_VERSION},
                },
            ),
            (
                models.KCIDBBuild.objects.get(id="redhat:public_build_2"),
                models.Issue.objects.get(description="Issue Public 2", kind__tag="Unidentified"),
                {
                    "id": "redhat:issue_4",
                    "origin": "redhat",
                    "comment": "Issue Public 2",
                    "report_url": "https://issue.public.2",
                    "version": 1609463420,
                    # NOTE: culprit should be omitted because issue kind is "unidentified"
                    "misc": {"is_public": True, "kcidb": DW_KCIDB_SCHEMA_VERSION},
                },
                {
                    "id": "redhat:incident_2",
                    "origin": "redhat",
                    "issue_id": "redhat:issue_4",
                    "issue_version": 1609463420,
                    "build_id": "redhat:public_build_2",
                    "present": True,
                    "misc": {"is_public": True, "kcidb": DW_KCIDB_SCHEMA_VERSION},
                },
            ),
        ]
        for instance, issue, serialized_issue, serialized_incident in cases:
            with self.subTest(instance=instance, issue=issue):
                scripts.notify_issueoccurrences_changed(instance, issues_ids={issue.id}, action="post_add")

                mocked_bulk_add.assert_has_calls([
                    mock.call(models.ObjectStatusEnum.UPDATED, "issue", [serialized_issue], {}),
                    mock.call(models.ObjectStatusEnum.UPDATED, "incident", [serialized_incident], {}),
                ])

            mocked_bulk_add.reset_mock()

    def test_notify_issueoccurrences_changed_rejects_invalid_action(self):
        """Test scripts.notify_issueoccurrences_changed rejects invalid action."""
        with self.assertRaisesMessage(NotImplementedError, "Unexpected action='invalid'"):
            scripts.notify_issueoccurrences_changed(self.checkout, [], action="invalid")

    @freeze_time("2024-01-01 00:00:00+00:00")
    @mock.patch('datawarehouse.scripts.misc.signals.kcidb_object')
    def test_not_ready_wont_notify_checkout_issueoccurrences_changed(self, mocked_signal):
        """Test checkouts that didn't report yet (ready_to_report=False) won't trigger a notification."""
        # prepare a checkout that shouldn't trigger the notification
        self.checkout.ready_to_report = False
        self.checkout.save(update_fields=["ready_to_report"])

        # Shouldn't notify no matter the outcome
        for outcome_changed in (True, False):
            with self.subTest(checkout_outcome_changed=outcome_changed):
                with self.assertLogs(logger=issues.LOGGER, level="DEBUG") as log_ctx:
                    issues.notify_checkout_issueoccurrences_changed(
                        self.checkout, checkout_outcome_changed=outcome_changed
                    )

                expected_log = (
                    f"DEBUG:{issues.LOGGER.name}:"
                    f"Skipping '{models.ObjectStatusEnum.CHECKOUT_ISSUEOCCURRENCES_CHANGED}'"
                    f" notification because {self.checkout!r} is not ready"
                )
                self.assertIn(expected_log, log_ctx.output)

                self.assertFalse(models.QueuedTask.objects.exists(), "Expected to skip scheduling task")
                mocked_signal.assert_not_called()

    @freeze_time("2024-01-01 00:00:00+00:00")
    @mock.patch("datawarehouse.scripts.misc.signals.kcidb_object")
    def test_basic_notify_checkout_issueoccurrences_changed(self, mocked_signal):
        """Test notify_checkout_issueoccurrences_changed works as expected."""
        # prepare a checkout that should trigger the notification
        self.checkout.ready_to_report = True
        self.checkout.save(update_fields=["ready_to_report"])

        for outcome_changed in (True, False):
            with self.subTest(checkout_outcome_changed=outcome_changed):
                common_args = {
                    "sender": "scripts.issues.notify_checkout_issueoccurrences_changed",
                    "status": models.ObjectStatusEnum.CHECKOUT_ISSUEOCCURRENCES_CHANGED,
                    "object_type": "checkout",
                    "misc": {"checkout_outcome_changed": outcome_changed},
                }
                signal_task_queryset = models.QueuedTask.objects.filter(
                    call_id=f"checkout_issueoccurrences_changed! signalize_kcidb_object({self.checkout.iid})",
                )

                issues.notify_checkout_issueoccurrences_changed(
                    self.checkout, checkout_outcome_changed=outcome_changed
                )

                self.assertTrue(signal_task_queryset.first(), "Expected to schedule task to notify")
                self.assertEqual(signal_task_queryset.get().calls_kwargs, {**common_args, "pks": [self.checkout.iid]})

                # After enough time the task should
                with freeze_time("2024-01-01 00:01:00"):
                    cron.RunQueuedTasks().entrypoint()
                self.assertFalse(signal_task_queryset.exists(), "Expected scheduled task to be deleted after running")

                mocked_signal.send.assert_called_once_with(**common_args, objects=[self.checkout])
                mocked_signal.reset_mock()

    @freeze_time("2024-01-01 00:00:00+00:00")
    @mock.patch("datawarehouse.scripts.misc.signals.kcidb_object")
    def test_issueoccurrences_signals_trigger_notify_checkout_issueoccurrences_changed(self, mocked_signal):
        """Test signals from changes to IssueOccurrence trigger notify_checkout_issueoccurrences_changed."""
        # prepare a checkout with some untriaged failures
        self.checkout.ready_to_report = True
        self.checkout.save(update_fields=["ready_to_report"])

        kcidb_test = self.checkout.tests.filter_triaged().filter(status=models.ResultEnum.FAIL).first()
        self.assertIsNotNone(kcidb_test, "Precondition: checkout needs at least one triaged failed test")

        issue = kcidb_test.issues.first()
        regression = models.Issue.objects.get(description="Issue Public")
        regression.resolved_at = self.checkout.start_time - timezone.timedelta(hours=1)
        regression.save(update_fields=["resolved_at"])

        self.assertNotEqual(issue.id, regression.id, "Precondition: test 2 different issues")

        common_args = {
            "sender": "scripts.issues.notify_checkout_issueoccurrences_changed",
            "status": models.ObjectStatusEnum.CHECKOUT_ISSUEOCCURRENCES_CHANGED,
            "object_type": "checkout",
            "misc": {"checkout_outcome_changed": True},  # the subtests are organized so that outcome toggles
        }
        signal_task_queryset = models.QueuedTask.objects.filter(
            call_id=f"checkout_issueoccurrences_changed! signalize_kcidb_object({self.checkout.iid})",
            calls_kwargs={**common_args, "pks": [self.checkout.iid]},
        )

        with self.subTest("Assert removing an IssueOccurrence calls the function"):
            with self.assertLogs(logger=issues.LOGGER, level="DEBUG") as log_ctx:
                kcidb_test.issues.remove(issue)

            expected_log = f"DEBUG:{issues.LOGGER.name}:Checkout tests outcome changed (before=True; now=False)"
            self.assertIn(expected_log, log_ctx.output)

            self.assertTrue(signal_task_queryset.exists(), "Expected to schedule task to notify")
            with freeze_time("2024-01-01 00:01:00"):
                cron.RunQueuedTasks().entrypoint()
            self.assertFalse(signal_task_queryset.exists(), "Expected scheduled task to be deleted after running")

            mocked_signal.send.assert_called_once_with(**common_args, objects=[self.checkout])

        mocked_signal.reset_mock()

        with self.subTest("Assert adding a new IssueOccurrence calls the function"):
            with self.assertLogs(logger=issues.LOGGER, level="DEBUG") as log_ctx:
                kcidb_test.issues.add(issue)

            expected_log = f"DEBUG:{issues.LOGGER.name}:Checkout tests outcome changed (before=False; now=True)"
            self.assertIn(expected_log, log_ctx.output)

            self.assertTrue(signal_task_queryset.exists(), "Expected to schedule task to notify")
            with freeze_time("2024-01-01 00:01:00"):
                cron.RunQueuedTasks().entrypoint()
            self.assertFalse(signal_task_queryset.exists(), "Expected scheduled task to be deleted after running")

            mocked_signal.send.assert_called_once_with(**common_args, objects=[self.checkout])

        mocked_signal.reset_mock()

        with self.subTest("Assert adding a regression changes the outcome"):
            with self.assertLogs(logger=issues.LOGGER, level="DEBUG") as log_ctx:
                kcidb_test.issues.add(regression)

            expected_log = f"DEBUG:{issues.LOGGER.name}:Checkout tests outcome changed (before=True; now=False)"
            self.assertIn(expected_log, log_ctx.output)

            self.assertTrue(signal_task_queryset.exists(), "Expected to schedule task to notify")
            with freeze_time("2024-01-01 00:01:00"):
                cron.RunQueuedTasks().entrypoint()
            self.assertFalse(signal_task_queryset.exists(), "Expected scheduled task to be deleted after running")

            mocked_signal.send.assert_called_once_with(**common_args, objects=[self.checkout])

        mocked_signal.reset_mock()

        with self.subTest("Assert removing a regression changes the outcome"):
            with self.assertLogs(logger=issues.LOGGER, level="DEBUG") as log_ctx:
                kcidb_test.issues.remove(regression)

            expected_log = f"DEBUG:{issues.LOGGER.name}:Checkout tests outcome changed (before=False; now=True)"
            self.assertIn(expected_log, log_ctx.output)

            self.assertTrue(signal_task_queryset.exists(), "Expected to schedule task to notify")
            with freeze_time("2024-01-01 00:01:00"):
                cron.RunQueuedTasks().entrypoint()
            self.assertFalse(signal_task_queryset.exists(), "Expected scheduled task to be deleted after running")

            mocked_signal.send.assert_called_once_with(**common_args, objects=[self.checkout])

        mocked_signal.reset_mock()

        with self.subTest("Assert clearing all IssueOccurrence calls the function"):
            with self.assertLogs(logger=issues.LOGGER, level="DEBUG") as log_ctx:
                kcidb_test.issues.clear()

            expected_log = f"DEBUG:{issues.LOGGER.name}:Checkout tests outcome changed (before=True; now=False)"
            self.assertIn(expected_log, log_ctx.output)

            self.assertTrue(signal_task_queryset.exists(), "Expected to schedule task to notify")
            with freeze_time("2024-01-01 00:01:00"):
                cron.RunQueuedTasks().entrypoint()
            self.assertFalse(signal_task_queryset.exists(), "Expected scheduled task to be deleted after running")

            mocked_signal.send.assert_called_once_with(**common_args, objects=[self.checkout])


class TestUpdateIssueOccurrencesRelatedCheckout(utils.TestCase):
    """
    Test update_issue_occurrences_related_checkout.

    Ensure IssueOccurrence.related_checkout is populated after
    assigning an issue to a KCIDB object.
    """

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up test."""
        self.issue = models.Issue.objects.last()

    def test_unexpected_type(self):
        """Test update_issue_occurrences_related_checkout with invalid instance type."""
        invalid_instance = self.issue
        with self.assertRaisesMessage(TypeError, f'Unhandled instance type {invalid_instance}'):
            scripts.update_issue_occurrences_related_checkout(invalid_instance, (self.issue.id, ))

    def test_checkout(self):
        """"Test update_issue_occurrences_related_checkout with checkout."""
        checkout = models.KCIDBCheckout.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_checkout=checkout)
        scripts.update_issue_occurrences_related_checkout(checkout, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_checkout, checkout)
        self.assertEqual(issue_occurrence.related_checkout, checkout)

    def test_build(self):
        """"Test update_issue_occurrences_related_checkout with build."""
        build = models.KCIDBBuild.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_build=build)
        scripts.update_issue_occurrences_related_checkout(build, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_build, build)
        self.assertEqual(issue_occurrence.related_checkout, build.checkout)

    def test_test(self):
        """"Test update_issue_occurrences_related_checkout with test."""
        test = models.KCIDBTest.objects.last()

        # Create IssueOccurrence to avoid triggering the signal
        models.IssueOccurrence.objects.create(issue=self.issue, kcidb_test=test)
        scripts.update_issue_occurrences_related_checkout(test, (self.issue.id, ))

        issue_occurrence = models.IssueOccurrence.objects.last()
        self.assertEqual(issue_occurrence.kcidb_test, test)
        self.assertEqual(issue_occurrence.related_checkout, test.build.checkout)

    def test_multiple(self):
        """"Test update_issue_occurrences_related_checkout when adding multiple objects."""
        test = models.KCIDBTest.objects.last()
        issues = models.Issue.objects.all()

        # Create IssueOccurrence to avoid triggering the signal
        for issue in issues:
            models.IssueOccurrence.objects.create(issue=issue, kcidb_test=test)
        scripts.update_issue_occurrences_related_checkout(test, (i.id for i in issues))

        for issue_occurrence in models.IssueOccurrence.objects.all():
            self.assertEqual(issue_occurrence.kcidb_test, test)
            self.assertEqual(issue_occurrence.related_checkout, test.build.checkout)


class TestUpdateIssueOccurrencesRegression(utils.TestCase):
    """Test update_issue_occurrences_regression."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    def setUp(self):
        """Set up test."""
        self.issue = models.Issue.objects.last()

    @staticmethod
    def _test_objects():
        objs = [
            (models.KCIDBCheckout.objects.get(id="redhat:public_checkout"), "kcidb_checkout"),
            (models.KCIDBBuild.objects.get(id="redhat:public_build_1"), "kcidb_build"),
            (models.KCIDBTest.objects.get(id="redhat:public_test_R"), "kcidb_test"),
            (models.KCIDBTestResult.objects.get(id="redhat:public_test_R_result_R"), "kcidb_testresult"),
        ]

        for obj, key in objs:
            # NOTE: KCIDBTestResult doesn't have start_time
            if key != 'kcidb_testresult':
                obj.start_time = timezone.now()
                obj.save(update_fields=["start_time"])

            yield key, obj

    def test_issue_not_resolved(self):
        """"Test when the issue is not resolved."""
        for key, obj in self._test_objects():
            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=self.issue, **{key: obj})
            scripts.update_issue_occurrences_regression(obj, (self.issue.id, ))

            issue_occurrence = models.IssueOccurrence.objects.last()
            self.assertFalse(issue_occurrence.is_regression)

    def test_issue_resolved_after(self):
        """"Test when the issue was resolved after the object run."""
        for key, obj in self._test_objects():
            # NOTE: KCIDBTestResult doesn't have start_time
            start_time = obj.test.start_time if isinstance(obj, models.KCIDBTestResult) else obj.start_time
            self.issue.resolved_at = start_time + datetime.timedelta(seconds=1)
            self.issue.save()

            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=self.issue, **{key: obj})
            scripts.update_issue_occurrences_regression(obj, (self.issue.id, ))

            issue_occurrence = models.IssueOccurrence.objects.last()
            self.assertFalse(issue_occurrence.is_regression)

    def test_issue_resolved_before(self):
        """"Test when the issue was resolved before the object run."""
        for key, obj in self._test_objects():
            # NOTE: KCIDBTestResult doesn't have start_time
            start_time = obj.test.start_time if isinstance(obj, models.KCIDBTestResult) else obj.start_time
            self.issue.resolved_at = start_time + datetime.timedelta(seconds=-1)
            self.issue.save()

            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=self.issue, **{key: obj})
            scripts.update_issue_occurrences_regression(obj, (self.issue.id, ))

            issue_occurrence = models.IssueOccurrence.objects.last()
            self.assertTrue(issue_occurrence.is_regression)

    def test_issue_resolved_before_no_start_time(self):
        """"Test when the issue was resolved before the object run but it has no start_time."""
        for key, obj in self._test_objects():
            obj.start_time = None
            obj.save()

            self.issue.resolved_at = timezone.now()
            self.issue.save()

            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=self.issue, **{key: obj})
            scripts.update_issue_occurrences_regression(obj, (self.issue.id, ))

            issue_occurrence = models.IssueOccurrence.objects.last()
            self.assertTrue(issue_occurrence.is_regression)


class TestSendRegressionNotification(utils.TestCase):
    """Test send_regression_notification."""

    fixtures = [
        'tests/fixtures/policies_for_group_abc.yaml',
        'tests/fixtures/basic.yaml',
        'tests/kcidb/fixtures/base_simple.yaml',
        'tests/fixtures/issues.yaml',
    ]

    @staticmethod
    def _test_objects():
        objs = [
            (models.KCIDBCheckout.objects.last(), 'kcidb_checkout'),
            (models.KCIDBBuild.objects.last(), 'kcidb_build'),
            (models.KCIDBTest.objects.last(), 'kcidb_test'),
            (models.KCIDBTestResult.objects.last(), 'kcidb_testresult'),
        ]

        for obj, key in objs:
            if key != 'kcidb_testresult':
                obj.start_time = timezone.now()
                obj.save()

            yield key, obj

    @mock.patch('datawarehouse.scripts.issues.settings.FF_NOTIFY_ISSUE_REGRESSION', True)
    @mock.patch('datawarehouse.scripts.issues.notify_issue_regression')
    def test_sends_only_regressions(self, mock_notify):
        """"Test it only sends notifications for regressions."""
        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)

        for key, obj in self._test_objects():
            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=issue_1, **{key: obj}, is_regression=True)
            models.IssueOccurrence.objects.create(issue=issue_2, **{key: obj}, is_regression=False)
            scripts.send_regression_notification(obj, (issue_1.id, issue_2.id))

            self.assertEqual(1, mock_notify.call_count)
            if isinstance(obj, models.KCIDBTestResult):
                mock_notify.assert_has_calls([mock.call(obj.test, issue_1)])
            else:
                mock_notify.assert_has_calls([mock.call(obj, issue_1)])
            mock_notify.reset_mock()

    @mock.patch('datawarehouse.scripts.issues.settings.FF_NOTIFY_ISSUE_REGRESSION', False)
    @mock.patch('datawarehouse.scripts.issues.notify_issue_regression')
    def test_verify_issue_regression_disable(self, mock_notify):
        """Test verify_issue_regression method. FF_NOTIFY_ISSUE_REGRESSION is disabled."""
        issue_1 = models.Issue.objects.get(id=1)
        issue_2 = models.Issue.objects.get(id=2)

        for key, obj in self._test_objects():
            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=issue_1, **{key: obj}, is_regression=True)
            models.IssueOccurrence.objects.create(issue=issue_2, **{key: obj}, is_regression=False)
            scripts.send_regression_notification(obj, (issue_1.id, issue_2.id))

            self.assertFalse(mock_notify.called)

    @mock.patch('datawarehouse.scripts.issues.settings.FF_NOTIFY_ISSUE_REGRESSION', True)
    @mock.patch('datawarehouse.scripts.issues.notify_issue_regression')
    def test_verify_issue_regression_retrigger(self, mock_notify):
        """Test verify_issue_regression method. Checkout is retriggered."""
        issue_1 = models.Issue.objects.get(id=1)

        for key, obj in self._test_objects():
            obj.checkout.retrigger = True
            obj.checkout.save()

            # Create IssueOccurrence to avoid triggering the signal
            models.IssueOccurrence.objects.create(issue=issue_1, **{key: obj}, is_regression=True)
            scripts.send_regression_notification(obj, (issue_1.id, ))

            self.assertFalse(mock_notify.called)


class TestRegressionNotifications(utils.TestCase):
    """Unit tests for issue_regression methods."""

    fixtures = [
        'tests/fixtures/basic.yaml',
        'tests/fixtures/issue_regressions.yaml',
    ]

    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_notify_issue_regression_subscribers(self, mock_send_mail):
        """Test notify_issue_regression method for subscribed users."""
        checkout = models.KCIDBCheckout.objects.get(id="redhat:checkout_1")
        issue = models.Issue.objects.get(description='issue_1')

        issues.notify_issue_regression(checkout, issue)

        # No user is subscribed
        self.assertFalse(mock_send_mail.called)

        # Subscribe all users to issue regressions
        for user in auth_models.User.objects.all():
            models.UserSubscriptions.objects.get_or_create(
                user=user,
                issue_regression_subscribed_at=datetime_bool(True),
                issue_regression_visibility=models.SubscriptionVisibility.CC
            )

        issues.notify_issue_regression(checkout, issue)

        users = auth_models.User.objects.filter(username__in=('user_4', 'user_5'))
        subject = f'{issue.kind.tag} | Issue #{issue.id} regression detected'
        message = (
            'Hello,\n\n'
            'You are receiving this email because a regression was detected\n'
            'and tagged in DataWarehouse.\n\n'
            f'Issue: {issue.description}\n'
            f'URL: {issue.web_url}\n'
            f'Seen in: {checkout.web_url}\n\n'
            '--\n'
            'The DataWarehouse team.\n'
        )

        mock_send_mail.assert_called_with(subject, message, mock.ANY)
        self.assertEqual(
            set(mock_send_mail.call_args_list[0][0][2]['cc']),
            set(u.email for u in users)
        )

    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_notify_issue_regression_subscribers_cc_bcc(self, mock_send_mail):
        """Test notify_issue_regression method for subscribed users respects visibility."""
        checkout = models.KCIDBCheckout.objects.get(id="redhat:checkout_1")
        issue = models.Issue.objects.get(description='issue_1')

        models.UserSubscriptions.objects.create(
            user=auth_models.User.objects.get(username='user_4'),
            issue_regression_subscribed_at=datetime_bool(True),
            issue_regression_visibility=models.SubscriptionVisibility.CC
        )

        models.UserSubscriptions.objects.create(
            user=auth_models.User.objects.get(username='user_5'),
            issue_regression_subscribed_at=datetime_bool(True),
            issue_regression_visibility=models.SubscriptionVisibility.BCC
        )

        issues.notify_issue_regression(checkout, issue)

        mock_send_mail.assert_called_with(
            mock.ANY, mock.ANY,
            {'cc': [auth_models.User.objects.get(username='user_4').email],
             'bcc': [auth_models.User.objects.get(username='user_5').email]}
        )

    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_notify_issue_regression_maintainers(self, mock_send_mail):
        """Test notify_issue_regression method for test maintainers."""
        test = models.KCIDBTest.objects.get(id="redhat:test_1")
        issue = models.Issue.objects.get(description='issue_1')

        issues.notify_issue_regression(test, issue)

        subject = f'{issue.kind.tag} | Issue #{issue.id} regression detected'
        message = (
            'Hello,\n\n'
            'You are receiving this email because a regression was detected\n'
            'and tagged in DataWarehouse.\n\n'
            f'Issue: {issue.description}\n'
            f'URL: {issue.web_url}\n'
            f'Seen in: {test.web_url}\n\n'
            '--\n'
            'The DataWarehouse team.\n'
        )

        mock_send_mail.assert_called_with(
            subject, message, {'to': [test.test.maintainers.first().email], 'cc': [], 'bcc': []}
        )

    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_notify_issue_regression_maintainers_scratch(self, mock_send_mail):
        """
        Test notify_issue_regression method for scratch checkouts.

        On scratch checkouts, only the submitter should be notified.
        """
        test = models.KCIDBTest.objects.get(id="redhat:test_1")
        issue = models.Issue.objects.get(description='issue_1')

        test.build.checkout.scratch = True
        test.build.checkout.submitter = models.Maintainer.objects.create(email='co-submitter@mail.com')
        test.build.checkout.save()

        models.UserSubscriptions.objects.create(
            user=auth_models.User.objects.get(username='user_4'),
            issue_regression_subscribed_at=datetime_bool(True),
            issue_regression_visibility=models.SubscriptionVisibility.CC
        )

        issues.notify_issue_regression(test, issue)

        # Notification is only sent to the submitter, not to the
        # test maintainer.
        mock_send_mail.assert_called_with(
            mock.ANY, mock.ANY,
            {'to': ['co-submitter@mail.com']}
        )
