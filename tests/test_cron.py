"""Test cron.py."""
import datetime
import threading
from unittest import mock

from django.contrib.auth import get_user_model
from django.test.utils import override_settings
from django.utils import timezone
from freezegun import freeze_time

from datawarehouse import cron
from datawarehouse import models
from tests import utils


class TestCron(utils.TestCase):
    """Test cron __init__.py."""

    def test_jobs(self):
        """Ensure that the list of jobs contains all the expected jobs."""
        expected_jobs = {
            cron.BuildSetupsFinishedCheckouts,
            cron.ClearExpiredSessions,
            cron.DeleteExpiredArtifacts,
            cron.DeleteOldNotConfirmedUsers,
            cron.ReadyToReportCheckouts,
            cron.RemoveExpiredCaptachas,
            cron.RunQueuedTasks,
            cron.SendAccountDeletionWarning,
            cron.SendQueuedEmails,
            cron.SendQueuedMessages,
            cron.TestsFinishedCheckouts,
            cron.UpdateLdapGroupMembers,
            cron.UpdatePrometheusMetrics5m,
            cron.UpdatePrometheusMetrics1h,
        }
        self.assertEqual(len(expected_jobs), len(cron.JOBS))
        self.assertEqual(
            set(job.__class__ for job in cron.JOBS),
            expected_jobs
        )

    @mock.patch('django.db.connection.close')
    def test_jobs_close_connections(self, mock_conn_close):
        """Ensure that all cron jobs close DB connection after running."""
        for job in cron.JOBS:
            job.entrypoint = mock.Mock()
            job.run()
            self.assertTrue(job.entrypoint.called)
            self.assertTrue(mock_conn_close.called)
            mock_conn_close.reset_mock()


class TestDeleteArtifacts(utils.TestCase):
    """Test cron.delete_expired_artifacts method."""

    @staticmethod
    def _expiry_in(valid_for):
        """Calculate expiry date."""
        return timezone.now() + datetime.timedelta(days=valid_for)

    def test_delete(self):
        """Test delete_expired_artifacts deletes expired artifacts."""
        models.Artifact.objects.bulk_create([
            # Expired
            models.Artifact(name='test_1', url='http://test_1', valid_for=1, expiry_date=self._expiry_in(-2)),
            models.Artifact(name='test_2', url='http://test_2', valid_for=1, expiry_date=self._expiry_in(-1)),
            # Still valid
            models.Artifact(name='test_3', url='http://test_3', valid_for=1, expiry_date=self._expiry_in(1)),
            models.Artifact(name='test_4', url='http://test_4', valid_for=1, expiry_date=self._expiry_in(2)),
        ])

        self.assertEqual(4, models.Artifact.objects.count())
        cron.DeleteExpiredArtifacts().entrypoint()
        self.assertEqual(2, models.Artifact.objects.count())

        # These would raise if not present.
        models.Artifact.objects.get(name='test_3')
        models.Artifact.objects.get(name='test_4')


class TestDeleteOldNotConfirmedUsers(utils.TestCase):
    """Test cron.delete_old_not_confirmed_users method"""

    @staticmethod
    def _shifted_days(days):
        """Calculate shifted date."""
        return timezone.now() + datetime.timedelta(days=days)

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=30)
    def test_delete(self):
        """Test if old invalid users get deleted"""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="old_test_1", date_joined=self._shifted_days(-32)),
            User(username="old_test_2", date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="old_test_3", date_joined=self._shifted_days(-15)),
            User(username="old_test_4", date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.DeleteOldNotConfirmedUsers().entrypoint()
        self.assertEqual(3, User.objects.count())

        User.objects.get(username="old_test_2")
        User.objects.get(username="old_test_3")
        User.objects.get(username="old_test_4")

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=None)
    def test_delete_disabled(self):
        """Test if old invalid users get deleted"""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="old_test_1", date_joined=self._shifted_days(-32)),
            User(username="old_test_2", date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="old_test_3", date_joined=self._shifted_days(-15)),
            User(username="old_test_4", date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.DeleteOldNotConfirmedUsers().entrypoint()
        self.assertEqual(4, User.objects.count())

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=30)
    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_send_deletion_notification(self, add_mock):
        """Test if email gets sent."""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="del_test_1", email="del1@mail.com",
                 date_joined=self._shifted_days(-32)),
            User(username="del_test_2", email="del2@mail.com",
                 date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="del_test_3", email="del3@mail.com",
                 date_joined=self._shifted_days(-15)),
            User(username="del_test_4", email="del4@mail.com",
                 date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.SendAccountDeletionWarning().entrypoint()

        # Check if mails have been sent to correct recipients
        self.assertEqual(2, add_mock.call_count)
        calls = [
            mock.call(mock.ANY, mock.ANY, "del1@mail.com"),
            mock.call(mock.ANY, mock.ANY, "del3@mail.com")
        ]
        add_mock.assert_has_calls(calls, any_order=True)

        # Check if we don't send multiple emails
        cron.SendAccountDeletionWarning().entrypoint()
        self.assertEqual(2, add_mock.call_count)

    @override_settings(FF_DEL_NOT_CONFIRMED_USERS_OLDER_THAN_DAYS=0)
    @mock.patch('datawarehouse.utils.EMAIL_QUEUE.add')
    def test_send_deletion_notification_disabled(self, add_mock):
        """Test if email doesn't get sent."""
        User = get_user_model()

        # Create test users
        User.objects.bulk_create([
            User(username="del_test_1", email="del1@mail.com",
                 date_joined=self._shifted_days(-32)),
            User(username="del_test_2", email="del2@mail.com",
                 date_joined=self._shifted_days(-32), last_login=self._shifted_days(-2)),
            User(username="del_test_3", email="del3@mail.com",
                 date_joined=self._shifted_days(-15)),
            User(username="del_test_4", email="del4@mail.com",
                 date_joined=self._shifted_days(-15), last_login=self._shifted_days(-2)),
        ])

        self.assertEqual(4, User.objects.count())
        cron.SendAccountDeletionWarning().entrypoint()
        self.assertEqual(0, add_mock.call_count)


class TestReadyToReportCheckouts(utils.TransactionTestCase):
    """Test cron.ReadyToReportCheckouts."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
        "tests/kcidb/fixtures/base_simple.yaml",
    )

    @mock.patch('datawarehouse.metrics.update_checkout_metrics')
    @mock.patch('datawarehouse.metrics.update_time_to_report')
    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_run(self, mock_signal, mock_update_time_to_report, mock_update_checkout_metrics):
        """Check messages are sent and the objects tagged as ready_to_report."""
        models.KCIDBCheckout.objects.update(ready_to_report=False, last_triaged_at=timezone.now())
        models.KCIDBBuild.objects.update(valid=True, last_triaged_at=timezone.now())
        models.KCIDBTest.objects.update(status='P', last_triaged_at=timezone.now())

        expected_checkout_ids = ["redhat:public_checkout_valid", "redhat:public_checkout_invalid"]
        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.filter_ready_to_report().values_list("id", flat=True).order_by("-id"),
            expected_checkout_ids,
        )

        with self.subTest("Assert checkouts ready to report trigger signal"):
            with self.assertLogs(logger=cron.jobs.LOGGER, level="INFO") as log_ctx:
                cron.ReadyToReportCheckouts().entrypoint()

            expected_log = f"INFO:{cron.jobs.LOGGER.name}:Found 2 checkouts ready to report: {expected_checkout_ids}"
            self.assertIn(expected_log, log_ctx.output)

            self.assertEqual(0, models.KCIDBCheckout.objects.filter_ready_to_report().count())

            mock_signal.send.assert_called_once_with(
                sender="cron.jobs.ReadyToReportCheckouts",
                status=models.ObjectStatusEnum.READY_TO_REPORT,
                object_type="checkout",
                objects=mock.ANY,  # matching queryset requires special asserts
            )
            self.assertQuerySetEqual(
                models.KCIDBCheckout.objects.filter(id__in=expected_checkout_ids),
                mock_signal.send.call_args.kwargs["objects"],
            )

        with self.subTest("Assert it updates the relevant metrics"):
            mock_update_time_to_report.assert_has_calls([
                mock.call(checkout) for checkout in models.KCIDBCheckout.objects.filter(id__in=expected_checkout_ids)
            ])
            mock_update_checkout_metrics.assert_has_calls([
                mock.call(checkout) for checkout in models.KCIDBCheckout.objects.filter(id__in=expected_checkout_ids)
            ])

        with self.subTest("Assert it logs when there's nothing to be done"):
            mock_signal.reset_mock()

            with self.assertLogs(logger=cron.jobs.LOGGER, level="INFO") as log_ctx:
                cron.ReadyToReportCheckouts().entrypoint()

            expected_log = f"INFO:{cron.jobs.LOGGER.name}:There are no KCIDBCheckouts ready to report."
            self.assertIn(expected_log, log_ctx.output)

            mock_signal.send.assert_not_called()


class TestBuildSetupsFinishedCheckouts(utils.TransactionTestCase):
    """Test cron.BuildSetupsFinishedCheckouts."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
        "tests/kcidb/fixtures/base_simple.yaml",
    )

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_run(self, mock_signal):
        """Check messages are sent when all the build setups finished."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBBuild.objects.update(kpet_tree_name='kpet_tree_name')

        expected_checkout_ids = ["redhat:public_checkout"]
        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.filter_build_setups_finished().values_list("id", flat=True).order_by("-id"),
            expected_checkout_ids,
        )
        with self.subTest("Assert checkouts with all build setups finished trigger signal"):
            with self.assertLogs(logger=cron.jobs.LOGGER, level="INFO") as log_ctx:
                cron.BuildSetupsFinishedCheckouts().entrypoint()
            expected_log = (
                f"INFO:{cron.jobs.LOGGER.name}:"
                f"Found 1 checkouts with all build setups finished: {expected_checkout_ids}"
            )
            self.assertIn(expected_log, log_ctx.output)

            self.assertEqual(0, models.KCIDBCheckout.objects.filter_build_setups_finished().count())

            mock_signal.send.assert_called_once_with(
                sender="cron.jobs.BuildSetupsFinishedCheckouts",
                status=models.ObjectStatusEnum.BUILD_SETUPS_FINISHED,
                object_type="checkout",
                objects=mock.ANY,  # matching queryset requires special asserts
            )
            self.assertQuerySetEqual(
                models.KCIDBCheckout.objects.filter(id__in=expected_checkout_ids),
                mock_signal.send.call_args.kwargs["objects"],
            )

        with self.subTest("Assert it logs when there's nothing to be done"):
            mock_signal.reset_mock()

            with self.assertLogs(logger=cron.jobs.LOGGER, level="INFO") as log_ctx:
                cron.BuildSetupsFinishedCheckouts().entrypoint()

            expected_log = f"INFO:{cron.jobs.LOGGER.name}:There are no KCIDBCheckouts with all build setups finished."
            self.assertIn(expected_log, log_ctx.output)

            mock_signal.send.assert_not_called()


class TestTestsFinishedCheckouts(utils.TransactionTestCase):
    """Test cron.TestsFinishedCheckouts."""

    fixtures = (
        "tests/fixtures/basic_policies.yaml",
        "tests/fixtures/basic.yaml",
        "tests/kcidb/fixtures/base_simple.yaml",
    )

    @mock.patch('datawarehouse.signals.kcidb_object')
    def test_run(self, mock_signal):
        """Check messages are sent when all the tests finished."""
        models.KCIDBCheckout.objects.update(notification_sent_build_setups_finished=False)
        models.KCIDBTest.objects.update(status='P')

        expected_checkout_ids = ["redhat:public_checkout"]
        self.assertQuerySetEqual(
            models.KCIDBCheckout.objects.filter_tests_finished().values_list("id", flat=True).order_by("-id"),
            expected_checkout_ids,
        )
        with self.subTest("Assert checkouts with all tests finished trigger signal"):
            with self.assertLogs(logger=cron.jobs.LOGGER, level="INFO") as log_ctx:
                cron.TestsFinishedCheckouts().entrypoint()
            expected_log = (
                f"INFO:{cron.jobs.LOGGER.name}:Found 1 checkouts with all tests finished: {expected_checkout_ids}"
            )
            self.assertIn(expected_log, log_ctx.output)

            self.assertEqual(0, models.KCIDBCheckout.objects.filter_tests_finished().count())

            mock_signal.send.assert_called_once_with(
                sender="cron.jobs.TestsFinishedCheckouts",
                status=models.ObjectStatusEnum.TESTS_FINISHED,
                object_type="checkout",
                objects=mock.ANY,
            )
            self.assertQuerySetEqual(
                models.KCIDBCheckout.objects.filter(id__in=expected_checkout_ids),
                mock_signal.send.call_args.kwargs["objects"],
            )

        with self.subTest("Assert it logs when there's nothing to be done"):
            mock_signal.reset_mock()

            with self.assertLogs(logger=cron.jobs.LOGGER, level="INFO") as log_ctx:
                cron.TestsFinishedCheckouts().entrypoint()

            expected_log = f"INFO:{cron.jobs.LOGGER.name}:There are no KCIDBCheckouts with all tests finished."
            self.assertIn(expected_log, log_ctx.output)

            mock_signal.send.assert_not_called()


class TestUpdatePrometheusMetrics5m(utils.TestCase):
    """Test cron.UpdatePrometheusMetrics5m."""

    @mock.patch('datawarehouse.metrics.update_unfinished_builds')
    @mock.patch('datawarehouse.metrics.update_unfinished_tests')
    def test_run(self, mock_update_unfinished_tests, mock_update_unfinished_builds):
        """Check metric update functions are called."""
        cron.UpdatePrometheusMetrics5m().entrypoint()
        self.assertTrue(mock_update_unfinished_builds.called)
        self.assertTrue(mock_update_unfinished_tests.called)


class TestUpdatePrometheusMetrics1h(utils.TestCase):
    """Test cron.UpdatePrometheusMetrics1h."""

    @mock.patch('datawarehouse.metrics.update_issues')
    def test_run(self, mock_update_issues):
        """Check metric update functions are called."""
        cron.UpdatePrometheusMetrics1h().entrypoint()
        self.assertTrue(mock_update_issues.called)


class TestSendQueuedMessages(utils.TestCase):
    """Test cron.SendQueuedMessages."""

    @override_settings(RABBITMQ_SEND_ENABLED=True)
    def test_run(self):
        """Check send messages function is called."""
        cron.jobs.utils.MSG_QUEUE = mock.Mock()
        cron.SendQueuedMessages().entrypoint()
        self.assertTrue(cron.jobs.utils.MSG_QUEUE.send.called)


class TestSendQueuedEmails(utils.TestCase):
    """Test cron.SendQueuedEmails."""

    @override_settings(EMAIL_SEND_ENABLED=True)
    def test_run(self):
        """Check send messages function is called."""
        cron.jobs.utils.EMAIL_QUEUE = mock.Mock()
        cron.SendQueuedEmails().entrypoint()
        self.assertTrue(cron.jobs.utils.EMAIL_QUEUE.send.called)

    @override_settings(EMAIL_SEND_ENABLED=False)
    def test_run_disabled(self):
        """Check send messages function is not called."""
        cron.jobs.utils.EMAIL_QUEUE = mock.Mock()
        cron.SendQueuedEmails().entrypoint()
        self.assertFalse(cron.jobs.utils.EMAIL_QUEUE.send.called)


class TestRunQueuedTasks(utils.TransactionTestCase):
    """Test cron.RunQueuedTasks."""

    @mock.patch('datawarehouse.models.task_models.celery.app.signature')
    def test_run(self, mock_celery):
        """Check QueuedTasks are run."""
        function_task_1 = mock.Mock()

        def mocked_signature(name):
            return {'task_1': function_task_1}[name]

        mock_celery.side_effect = mocked_signature

        with freeze_time("2010-01-02 09:00:00"):
            models.QueuedTask.create(
                name='task_1',
                call_id='task_1_1',
                call_kwargs=[{'foo': 'bar'}]
            )

        # Too early, shouldn't run task_1
        with freeze_time("2010-01-02 09:01:00"):
            cron.RunQueuedTasks().entrypoint()

        self.assertFalse(function_task_1.called)

        # Enough time passed, should run task_1
        with freeze_time("2010-01-02 09:10:00"):
            cron.RunQueuedTasks().entrypoint()

        self.assertTrue(function_task_1.called)
        function_task_1.assert_called_with([{'foo': 'bar'}])

    @mock.patch('datawarehouse.models.task_models.celery.app.signature')
    def test_run_failure(self, mock_celery):
        """Check QueuedTasks behaviour on failure."""
        function_task_1 = mock.Mock(side_effect=Exception)
        function_task_2 = mock.Mock()

        def mocked_signature(name):
            return {
                'task_1': function_task_1,
                'task_2': function_task_2,
            }[name]

        mock_celery.side_effect = mocked_signature

        with freeze_time("2010-01-02 09:00:00"):
            task_1 = models.QueuedTask.create(
                name='task_1',
                call_id='task_1',
                call_kwargs={'foo': 'bar'}
            )
            models.QueuedTask.create(
                name='task_2',
                call_id='task_2',
                call_kwargs={'foo': 'bar'}
            )

        with freeze_time("2010-01-02 09:10:00"):
            with self.assertLogs(logger=models.task_models.LOGGER, level="ERROR") as log_ctx:
                cron.RunQueuedTasks().entrypoint()
            self.assertTrue(log_ctx.output[0].startswith(
                f"ERROR:{models.task_models.LOGGER.name}:Error running QueuedTask ({task_1})"
            ))

        self.assertTrue(function_task_1.called)
        self.assertTrue(function_task_2.called)

        # Task 1 failed so it still exists, will be retried next iteration.
        self.assertTrue(
            models.QueuedTask.objects.filter(name='task_1', call_id='task_1').exists()
        )

        # Task 2 run successfully even after Task 1 failed
        self.assertFalse(
            models.QueuedTask.objects.filter(name='task_2', call_id='task_2').exists()
        )

    @mock.patch('datawarehouse.models.task_models.celery.app.signature')
    def test_run_taking_too_long(self, mock_celery):
        """Check QueuedTasks behaviour on failure."""
        logger = cron.jobs.LOGGER

        # prepare the mocked serializer

        unfreeze_runner = threading.Event()
        started_queued_task = threading.Event()

        def mocked_signature(name):
            started_queued_task.set()
            self.assertTrue(unfreeze_runner.wait(timeout=3), "Timeout while waiting for ")
            return mock.Mock(name=name)

        mock_celery.side_effect = mocked_signature

        def run_queued_task():
            from django.db import connections  # noqa: PLC0415 (Threads need manual connection handling)

            cron.RunQueuedTasks().entrypoint()
            connections.close_all()

        try:
            threaded_runner = threading.Thread(target=run_queued_task, daemon=True)

            with freeze_time("2010-01-02 09:00:00"):
                queued_task = models.QueuedTask.create(
                    name='task',
                    call_id='task',
                    call_kwargs={'foo': 'bar'},
                    run_in_minutes=0,
                )
                threaded_runner.start()
                self.assertTrue(started_queued_task.wait(timeout=3), "Timeout while waiting for queued task to start")

            # Assert thread has started the QueuedTask
            queued_task.refresh_from_db()
            self.assertIsNotNone(queued_task.start_time, "Precondition: Expected task to have started")

            with freeze_time("2010-01-02 09:01:01"), self.assertLogs(logger=logger, level='WARNING') as log_ctx:
                # unfreeze runner and then wait for it to finish and log using the mocked now()
                unfreeze_runner.set()
                threaded_runner.join()
        finally:
            threaded_runner.join()  # fallback to wait for the thread to finish in case of exceptions

        expected_log = (
            f'WARNING:{logger.name}:'
            f'QueuedTask ({queued_task}) took too long to run (61 seconds).'
        )
        self.assertIn(expected_log, log_ctx.output)
