"""Timetag."""
import datetime

from django import template

register = template.Library()


def timetag(timestamp):
    """Convert seconds into HH:MM:SS."""
    try:
        timestamp_float = float(timestamp)
    except (ValueError, TypeError):
        return '-'

    time = datetime.datetime.fromtimestamp(timestamp_float)
    return time.strftime('%H:%M:%S')


register.filter(timetag)
