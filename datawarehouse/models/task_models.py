"""Models for scheduled tasks."""
from cki_lib.logger import get_logger
from django.conf import settings
from django.db import models
from django.db import transaction
from django.utils import timezone
from django_prometheus.models import ExportModelOperationsMixin as EMOM

from datawarehouse import celery
from datawarehouse.models.utils import AuthorizedQuerySet

LOGGER = get_logger(__name__)


class QueuedTaskQuerySet(AuthorizedQuerySet):
    """QuerySet for QueuedTask."""

    def filter_ready_to_run(self):
        """Filter QueuedTask ready to run."""
        # Queued tasks that started long enough ago should run again
        return self.filter(run_at__lte=timezone.now()).exclude(
            start_time__gt=timezone.now() - timezone.timedelta(minutes=settings.RETRY_QUEUED_TASK_TIMEOUT)
        )


class QueuedTask(EMOM('queued_task'), models.Model):
    """Model for QueuedTask."""

    # Arbitrary ID to identify unique calls that haven't started yet
    call_id = models.CharField(max_length=None)
    # Name matching celery task name
    name = models.CharField(max_length=None)
    # List of kwargs. Each time the task is called, the call kwargs are appended to the list.
    calls_kwargs = models.JSONField(default=list)
    run_at = models.DateTimeField()
    # Timestamp for the moment the task started running.
    # NOTE: Completed tasks are deleted, so durations over one minute signalize issues.
    start_time = models.DateTimeField(null=True, blank=True)

    objects = QueuedTaskQuerySet.as_manager()

    class Meta:
        """Indices and constraints."""

        constraints = [
            # call_id should be unique, as long as the task didn't start
            models.UniqueConstraint(fields=["call_id"], condition=models.Q(start_time__isnull=True),
                                    name="unique_call_id_when_start_time_isnull")
        ]

    def __str__(self):
        """Return __str__ formatted."""
        return f'{self.name} [{self.call_id}]'

    @classmethod
    def create(cls, name, call_id, call_kwargs, *,
               run_in_minutes=5):
        """Create task. If call_kwargs is a list and the task hasn't started yet, append them to it."""
        run_at = timezone.now() + timezone.timedelta(minutes=run_in_minutes)

        with transaction.atomic():
            task, created = cls.objects.select_for_update().get_or_create(
                call_id=call_id,
                start_time=None,
                defaults={
                    'name': name,
                    'calls_kwargs': call_kwargs,
                    'run_at': run_at,
                }
            )

            if not created:
                task.run_at = max(run_at, task.run_at)
                if isinstance(call_kwargs, list):
                    task.calls_kwargs.extend(call_kwargs)
                task.save(update_fields=['run_at', 'calls_kwargs'])

        LOGGER.info("QueuedTask: %s %s. Next run: %s",
                    task, 'Created' if created else 'Postponed', task.run_at)

        return task

    def run(self):
        """
        Run the scheduled task.

        The celery task is called with all the calls_kwargs stored for the
        task.
        """
        with transaction.atomic():
            # quickly mark the task as started and release the lock
            updated = (
                QueuedTask.objects.filter_ready_to_run().filter(pk=self.pk)
                .select_for_update(skip_locked=True)
                .update(start_time=timezone.now())
            )
            if updated:
                LOGGER.debug("QueuedTask (%s) selected to run.", self)
            else:
                LOGGER.error("Tried to run QueuedTask (%s), but it's not ready to run or already running", self)
                return

        LOGGER.info("QueuedTask: Running %s", self)
        try:
            task_function = celery.app.signature(self.name)
            if isinstance(self.calls_kwargs, dict):
                task_function(**self.calls_kwargs)
            else:
                task_function(self.calls_kwargs)
        except Exception:  # pylint: disable=broad-except
            LOGGER.exception('Error running QueuedTask (%s), will be retried on a new task.', self)
            # retry as a new task or append, which could mean appending calls_kwargs to an existent scheduled task
            QueuedTask.create(
                name=self.name,
                call_id=self.call_id,
                call_kwargs=self.calls_kwargs,
                run_in_minutes=1,
            )

        self.delete()
        LOGGER.info("QueuedTask (%s) purged.", self)
