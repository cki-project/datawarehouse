"""Issues scripts."""
from collections import defaultdict
import typing

from cki_lib.logger import get_logger
from django.conf import settings
from django.template import loader

from datawarehouse import models
from datawarehouse import utils

LOGGER = get_logger(__name__)

if typing.TYPE_CHECKING:
    from datawarehouse.models import KCIDBCheckout


def _issue_occurrence_filter(instance, issues_ids):
    """Return a dict to filter the given instance and issues within the IssueOccurrences."""
    if isinstance(instance, models.KCIDBCheckout):
        issue_occurrence_filter = {'kcidb_checkout': instance}
    elif isinstance(instance, models.KCIDBBuild):
        issue_occurrence_filter = {'kcidb_build': instance}
    elif isinstance(instance, models.KCIDBTest):
        issue_occurrence_filter = {'kcidb_test': instance}
    elif isinstance(instance, models.KCIDBTestResult):
        issue_occurrence_filter = {'kcidb_testresult': instance}
    else:
        raise TypeError(f'Unhandled instance type {instance}')

    issue_occurrence_filter.update({'issue__id__in': issues_ids})
    return issue_occurrence_filter


def notify_checkout_issueoccurrences_changed(checkout: "KCIDBCheckout", *, checkout_outcome_changed: bool):
    """Send a message in case IssueOccurrences within the checkout are updated."""
    if not checkout.ready_to_report:
        LOGGER.debug(
            "Skipping '%s' notification because %r is not ready",
            models.ObjectStatusEnum.CHECKOUT_ISSUEOCCURRENCES_CHANGED, checkout)
        return None

    misc = {"checkout_outcome_changed": checkout_outcome_changed} if checkout_outcome_changed is not None else {}

    task = models.QueuedTask.create(
        call_id=f"checkout_issueoccurrences_changed! signalize_kcidb_object({checkout.iid})",
        name="datawarehouse.scripts.misc.signalize_kcidb_object",
        call_kwargs={
            "sender": "scripts.issues.notify_checkout_issueoccurrences_changed",
            "status": models.ObjectStatusEnum.CHECKOUT_ISSUEOCCURRENCES_CHANGED,
            "object_type": "checkout",
            "pks": [checkout.iid],
            "misc": misc,
        },
        run_in_minutes=1,  # just debounce a little bit
    )

    return task


def notify_issueoccurrences_changed(instance, issues_ids, action):
    """
    Send notifications for each checkouts that got their issue occurrences updated.

    Args:
        instance: KCIDB object that had its issue occurrences changed
        issues_ids: ids of the Issue instances in the issue occurrences that changed
        action: descriptor of the change done to the queryset
    """
    issue_occurrence_filter = _issue_occurrence_filter(instance, issues_ids)
    issueoccurrences = models.IssueOccurrence.objects.filter(**issue_occurrence_filter)

    match action:
        case "post_add":
            previous_issue_occurrences = models.IssueOccurrence.objects.exclude(pk__in=issueoccurrences)
            new_issue_occurrences = models.IssueOccurrence.objects.all()
        case "pre_remove" | "pre_clear":
            previous_issue_occurrences = models.IssueOccurrence.objects.all()
            new_issue_occurrences = models.IssueOccurrence.objects.exclude(pk__in=issueoccurrences)
        case _:
            raise NotImplementedError(f"Unexpected {action=}")

    checkout = instance.checkout

    previous_tests_ok = not (
        checkout.tests.filter_untriaged_blocking(issue_occurrences=previous_issue_occurrences).exists()
        or previous_issue_occurrences.filter(is_regression=True).exists()
    )

    new_tests_ok = not (
        checkout.tests.filter_untriaged_blocking(issue_occurrences=new_issue_occurrences).exists()
        or new_issue_occurrences.filter(is_regression=True).exists()
    )

    outcome_changed = previous_tests_ok != new_tests_ok
    LOGGER.debug(
        "Checkout tests outcome %s (before=%r; now=%r)",
        "changed" if outcome_changed else "remained the same",
        previous_tests_ok,
        new_tests_ok,
    )
    notify_checkout_issueoccurrences_changed(checkout, checkout_outcome_changed=outcome_changed)

    # send messages with issues and incidents to KCIDB
    if action == "post_add":
        issues = [
            utils.nested_dict_from_double_underscore(utils.clean_dict(issue))
            for issue in models.Issue.objects.filter(id__in=issues_ids).kcidb_values()
        ]
        incidents = [
            utils.nested_dict_from_double_underscore(utils.clean_dict(occur))
            for occur in issueoccurrences.kcidb_values()
        ]
        utils.MSG_QUEUE.bulk_add(models.ObjectStatusEnum.UPDATED, "issue", issues, {})
        utils.MSG_QUEUE.bulk_add(models.ObjectStatusEnum.UPDATED, "incident", incidents, {})


def update_issue_occurrences_related_checkout(instance, issues_ids):
    """
    Update IssueOccurrence.related_checkout parameter.

    Called on post_add, this function sets the related_checkout parameter
    on the created IssueOccurrence objects.
    """
    # The call arguments don't contain the IssueOccurrence so it's
    # necessary to query it with the Issue and the KCIDB object.
    issue_occurrence_filter = _issue_occurrence_filter(instance, issues_ids)

    related_checkout = instance.checkout

    models.IssueOccurrence.objects.filter(
        **issue_occurrence_filter
    ).update(
        related_checkout=related_checkout
    )


def update_issue_occurrences_regression(instance, issues_ids):
    """Set regression=True if the IssueOccurrence is a regression."""
    # The call arguments don't contain the IssueOccurrence so it's
    # necessary to query it with the Issue and the KCIDB object.
    issue_occurrence_filter = _issue_occurrence_filter(instance, issues_ids)

    issue_occurrence_filter.update({
        'issue__resolved_at__isnull': False,
    })

    # Always retrieve the checkout start time to compare with regression
    # resolve time, as the kernel code and a lot of CKI packages are retrieved
    # at the start of the run, and thus the start time of the object does not
    # reflect when it was possible for a fix to go in.
    related_checkout = instance.checkout
    if start_time := related_checkout.start_time:
        # Best effort, only compare resolved_at date if the object has one.
        issue_occurrence_filter.update({
            'issue__resolved_at__lt': start_time,
        })

    models.IssueOccurrence.objects.filter(
        **issue_occurrence_filter,
    ).update(
        is_regression=True
    )


def send_regression_notification(instance, issues_ids):
    """Call notify_issue_regression if necessary."""
    if not settings.FF_NOTIFY_ISSUE_REGRESSION:
        return

    if instance.checkout.retrigger:
        # Do not send regression notifications for retriggers
        return

    # The call arguments don't contain the IssueOccurrence so it's
    # necessary to query it with the Issue and the KCIDB object.
    issue_occurrence_filter = _issue_occurrence_filter(instance, issues_ids)
    if isinstance(instance, models.KCIDBTestResult):
        instance = instance.test

    issue_occurrences = models.IssueOccurrence.objects.filter(
        **issue_occurrence_filter,
        is_regression=True
    )

    for issue_occurrence in issue_occurrences:
        notify_issue_regression(
            instance,
            issue_occurrence.issue,
        )


def get_issue_regression_notification_recipients(obj, issue):
    """
    Get the recipients for an issue regression.

    Depending on users settings, checkout type and other factors,
    calculate who needs to be notified about this regression.
    """
    recipients = defaultdict(set)

    if submitter := obj.checkout.submitter:
        recipients['to'].add(submitter.email)

    if obj.checkout.scratch:
        # On scratch checkouts, only notify the submitter
        return recipients

    # Filter users with authorization to read the issue
    users = issue.users_read_authorized
    # Exclude users not subscribed to the issue regression
    users = users.exclude(subscriptions__issue_regression_subscribed_at=None)
    # Filter users with authorization to read the object
    users = [u for u in users if u in obj.users_read_authorized]

    recipients['cc'].update(
        u.email for u in users
        if u.subscriptions.issue_regression_visibility == models.SubscriptionVisibility.CC
    )

    recipients['bcc'].update(
        u.email for u in users
        if u.subscriptions.issue_regression_visibility == models.SubscriptionVisibility.BCC
    )

    if isinstance(obj, models.KCIDBTest):
        maintainers_emails = obj.test.maintainers.values_list('email', flat=True)
        recipients['to'].update(maintainers_emails)

    return recipients


def notify_issue_regression(obj, issue):
    """Notify that an issue regression was detected."""
    email_template = loader.get_template('email_issue_regression.html')
    email_context = {
        'issue': issue,
        'obj': obj,
    }

    email_subject = f'{issue.kind.tag} | Issue #{issue.id} regression detected'
    email_message = email_template.render(email_context, {})

    recipients = get_issue_regression_notification_recipients(obj, issue)
    # Sets are not serializable, convert to list.
    recipients = {k: list(v) for k, v in recipients.items()}

    if any(recipients.values()):
        LOGGER.info("notify_issue_regression: issue_id=%d recipients=%s",
                    issue.id, recipients)
        utils.EMAIL_QUEUE.add(
            email_subject, email_message, recipients
        )
