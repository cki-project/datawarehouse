"""Prometheus metrics definition."""
import datetime

from celery import shared_task
from cki_lib.logger import get_logger
from django.contrib.auth import get_user_model
from django.db.models import Count
from django.db.models import Q
from django.utils import timezone
import prometheus_client

from datawarehouse import models

LOGGER = get_logger(__name__)

BUCKETS_RANGE = [0.1, 0.2, 0.4, 1, 2, 4]

TIME_TO_BUILD = prometheus_client.Histogram(
    'cki_time_to_build',
    'Time between a checkout is started and a build has finished',
    ['architecture'],
    # Target: 1h
    buckets=[3600 * r for r in BUCKETS_RANGE]
)

TIME_TO_REPORT = prometheus_client.Histogram(
    'cki_time_to_report',
    'Time between a checkout is started and a ready_to_report message is sent',
    # Target: 24h
    buckets=[3600 * 24 * r for r in BUCKETS_RANGE]
)

UNFINISHED_BUILDS = prometheus_client.Gauge(
    'cki_unfinished_builds',
    'Number of builds planned not yet finished.'
)

UNFINISHED_TESTS = prometheus_client.Gauge(
    'cki_unfinished_tests',
    'Number of tests planned not yet finished.'
)

ISSUES = prometheus_client.Gauge(
    'cki_issues',
    'Number of issues.',
    ['resolved']
)

ISSUE_REGEXES = prometheus_client.Gauge(
    'cki_issues_regexes',
    'Number of issue regexes.'
)

ISSUE_TAGGER = prometheus_client.Gauge(
    'cki_issues_tagger',
    'Number of issues tagged by each user.',
    ['username']
)

BASELINES_CHECKOUTS = prometheus_client.Gauge(
    'cki_baselines_checkout_valid_count',
    'Status of the baseline checkouts',
    ['known_issues', 'valid']
)

BASELINES_BUILDS = prometheus_client.Gauge(
    'cki_baselines_builds_valid_count',
    'Status of the baseline builds',
    ['architecture', 'known_issues', 'valid']
)

BASELINES_TESTS = prometheus_client.Gauge(
    'cki_baselines_tests_status_count',
    'Status of the baseline tests',
    ['architecture', 'known_issues', 'status']
)

CHECKOUTS_VALID = prometheus_client.Counter(
    'cki_dw_checkouts_valid',
    'Count of valid/invalid checkouts',
    ['valid', 'known_issues']
)

BUILDS_VALID = prometheus_client.Counter(
    'cki_dw_builds_valid',
    'Count of valid/invalid builds',
    ['valid', 'known_issues', 'architecture']
)

TESTS_STATUS = prometheus_client.Counter(
    'cki_dw_tests_status',
    'Count of tests on each status',
    ['status', 'known_issues', 'architecture', 'waived']
)


@shared_task
def update_time_to_build(build):
    """Update cki_time_to_build metric."""
    LOGGER.info("Updating cki_time_to_build metric on %r", build)
    build = (
        build
        if isinstance(build, models.KCIDBBuild) else
        models.KCIDBBuild.objects.get(iid=build)
    )
    try:
        time_to_build = (
            (build.start_time - build.checkout.start_time).total_seconds() + build.duration
        )
    except TypeError:
        # Some of the fields are missing
        pass
    else:
        # Consistency check to exclude incorrect timestamps
        if time_to_build > 0:
            TIME_TO_BUILD.labels(build.get_architecture_display()).observe(time_to_build)


@shared_task
def update_time_to_report(checkout):
    """Update cki_time_to_report metric."""
    LOGGER.info("Updating cki_time_to_report metric on %r", checkout)
    checkout = (
        checkout
        if isinstance(checkout, models.KCIDBCheckout) else
        models.KCIDBCheckout.objects.get(iid=checkout)
    )
    try:
        time_to_report = (timezone.now() - checkout.start_time).total_seconds()
    except TypeError:
        # Some of the fields are missing
        pass
    else:
        # Consistency check to exclude incorrect timestamps
        if time_to_report > 0:
            TIME_TO_REPORT.observe(time_to_report)


@shared_task
def update_unfinished_builds():
    """Update cki_unfinished_builds metric."""
    LOGGER.info('Updating cki_unfinished_builds metric')
    UNFINISHED_BUILDS.set(
        models.KCIDBBuild.objects.filter(valid=None).count()
    )


@shared_task
def update_unfinished_tests():
    """Update cki_unfinished_tests metric."""
    LOGGER.info('Updating cki_unfinished_tests metric')
    UNFINISHED_TESTS.set(
        models.KCIDBTest.objects.filter(status=None).count()
    )


@shared_task
def update_issues():
    """Update metrics related to issues."""
    LOGGER.info('Updating issues metrics')

    ISSUES.labels('true').set(
        models.Issue.objects.filter(resolved_at__isnull=False).count()
    )

    ISSUES.labels('false').set(
        models.Issue.objects.filter(resolved_at__isnull=True).count()
    )

    ISSUE_REGEXES.set(
        models.IssueRegex.objects.count()
    )

    all_taggers = (
        models.IssueOccurrence.objects
        .exclude(created_by=None)  # Issues tagged before created_by was added
        .values('created_by')
        .order_by('created_by')
        .annotate(tagged_count=Count('id'))
    )
    for tagger in all_taggers:
        user = get_user_model().objects.get(id=tagger['created_by'])
        ISSUE_TAGGER.labels(user.username).set(tagger['tagged_count'])


@shared_task
def update_baselines():
    """Update metrics related to baselines."""
    LOGGER.info('Updating baselines metrics')

    last_2_months = timezone.now() - datetime.timedelta(days=60)
    checkouts_iids = (
        models.KCIDBCheckout.objects
        .filter(
            start_time__gte=last_2_months,
            scratch=False,
        )
        .exclude(git_repository_url=None)  # Remove checkouts without necessary data
        .order_by('git_repository_url', 'git_repository_branch', 'tree__name', '-iid')
        .distinct('git_repository_url', 'git_repository_branch', 'tree__name')
        .values_list('iid', flat=True)
    )

    # Include Checkouts that have no git_repository_url grouping them by kpet_tree_name
    # and package_name. This is intended to include Brew official builds.
    no_git_url_checkouts = (
        models.KCIDBCheckout.objects
        .filter(
            start_time__gte=last_2_months,
            scratch=False,
        )
        .order_by('kcidbbuild__kpet_tree_name', 'kcidbbuild__package_name', '-iid')
        .distinct('kcidbbuild__kpet_tree_name', 'kcidbbuild__package_name')
        .values_list('iid', flat=True)
    )

    checkouts = (
        models.KCIDBCheckout.objects
        .annotated_by_architecture()
        .filter(iid__in=list(checkouts_iids) + list(no_git_url_checkouts))
        .order_by('git_repository_url', 'git_repository_branch', 'kcidbbuild__kpet_tree_name', 'tree__name')
        .prefetch_related('kcidbbuild_set')
        .distinct()
    )

    builds = (
        models.KCIDBBuild.objects
        .filter(checkout__in=checkouts)
    )

    tests = (
        models.KCIDBTest.objects
        .filter(build__in=builds)
    )

    checkout_metric_cases = [
        (f'{valid}_{known_issues}', valid, known_issues)
        for valid in (True, False)
        for known_issues in (True, False)
    ]

    build_metric_cases = [
        (f'{architecture.name}_{valid}_{known_issues}', architecture, valid, known_issues)
        for architecture in models.ArchitectureEnum
        for valid in (True, False)
        for known_issues in (True, False)
    ]

    test_metric_cases = [
        (f'{architecture.name}_{status.name}_{known_issues}', architecture, status, known_issues)
        for architecture in models.ArchitectureEnum
        for status in models.ResultEnum
        for known_issues in (True, False)
    ]

    checkouts = checkouts.aggregate(**{
        key: Count(
            'iid',
            filter=Q(
                valid=valid,
                issueoccurrence__isnull=(not known_issues)
            )
        )
        for key, valid, known_issues in checkout_metric_cases
    })

    builds = builds.aggregate(**{
        key: Count(
            'iid',
            filter=Q(
                architecture=architecture,
                valid=valid,
                issueoccurrence__isnull=(not known_issues)
            )
        )
        for key, architecture, valid, known_issues in build_metric_cases
    })

    tests = tests.aggregate(**{
        key: Count(
            'iid',
            filter=Q(
                status=status.value,
                build__architecture=architecture.value,
                issueoccurrence__isnull=(not known_issues),
                waived=False,  # We don't care about waived tests in any case here
            )
        )
        for key, architecture, status, known_issues in test_metric_cases
    })

    for key, valid, known_issues in checkout_metric_cases:
        BASELINES_CHECKOUTS.labels(
            valid=valid,
            known_issues=known_issues,
        ).set(
            checkouts[key]
        )

    for key, architecture, valid, known_issues in build_metric_cases:
        BASELINES_BUILDS.labels(
            architecture=architecture.name,
            valid=valid,
            known_issues=known_issues,
        ).set(
            builds[key]
        )

    for key, architecture, status, known_issues in test_metric_cases:
        BASELINES_TESTS.labels(
            architecture=architecture.name,
            status=status.name,
            known_issues=known_issues,
        ).set(
            tests[key]
        )


@shared_task
def update_checkout_metrics(checkout):
    """Update metrics related to a checkout."""
    LOGGER.info("Updating checkout metrics on %r", checkout)
    checkout = (
        checkout
        if isinstance(checkout, models.KCIDBCheckout) else
        models.KCIDBCheckout.objects.get(iid=checkout)
    )

    CHECKOUTS_VALID.labels(
        valid=checkout.valid,
        known_issues=checkout.issues.exists()
    ).inc()

    for build in checkout.kcidbbuild_set.all():
        BUILDS_VALID.labels(
            valid=build.valid,
            known_issues=build.issues.exists(),
            architecture=build.get_architecture_display(),
        ).inc()

        for test in build.kcidbtest_set.all():
            TESTS_STATUS.labels(
                status=test.get_status_display(),
                known_issues=test.issues.exists(),
                architecture=build.get_architecture_display(),
                waived=test.waived or False,
            ).inc()
