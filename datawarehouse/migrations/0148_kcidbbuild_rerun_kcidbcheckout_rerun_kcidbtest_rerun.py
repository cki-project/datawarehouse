# Generated by Django 5.1.5 on 2025-02-07 04:40

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datawarehouse', '0147_issue_tags'),
    ]

    operations = [
        migrations.AddField(
            model_name='kcidbbuild',
            name='rerun',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='supersedes', to='datawarehouse.kcidbbuild'),
        ),
        migrations.AddField(
            model_name='kcidbcheckout',
            name='rerun',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='supersedes', to='datawarehouse.kcidbcheckout'),
        ),
        migrations.AddField(
            model_name='kcidbtest',
            name='rerun',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='supersedes', to='datawarehouse.kcidbtest'),
        ),
    ]
